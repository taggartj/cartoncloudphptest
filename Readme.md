#How To Set up

This is a symphony application living in docker.
so you must, have docker installed,  clone this repo and run: 

```docker-compose up -d```

then run:

```composer install```

if you dont have composer then 
```docker exec -it sf4_php bash```

and ```cd sf4``` then ```composer install```



probably go get a coffee as it will pull some images. 

once done goto "http://localhost:8989"

or change the port number to anything you you changed in the docker-compose file.

you should see: 

```{"message":"Taggart Jensen - Test for Carton Cloud"}```



#API Usage

Assuming you are using poster or something similar you,  


ENDPOINT: "/test"
METHOD: "POST"

Headers:
- Authorization: Basic ZGVtbzpwd2QxMjM0
- Content-Type: application/json

Post Body:


```
{
	"purchase_order_ids": [2344, 2345, 2346]
}
```

Response Sample: 
```
{
    "result": [
        {
            "product_type_id": "1",
            "total": 41.5
        },
        {
            "product_type_id": "2",
            "total": 13.80
        },
        {
            "product_type_id": "3",
            "total": 25
        }
    ]
}
```

Please see Controller:
"src/Controller/MockRestApiController.php"


Auth was set in Test requirement doc. 
also see "config/packages/security.yaml"


#To Test Part 2 
Please see: 

"BearClaw/Warehousing/PurchaseOrderService.php"

or goto http://localhost:8989/totals

and look at 


"src/Controller/DefaultController.php"
('testTotals')



##Unit Testing 
Included php unit
First pop the shell in the php docker container

```docker exec -it sf4_php bash```

goto the web root aka ```cd sf4```

then run ```phpunit```

Test class "tests/TestControllerTest.php"





