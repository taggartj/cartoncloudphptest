<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use BearClaw\Warehousing\TotalsCalculator;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends AbstractController {
  /**
   * Default method
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
    public function index()
    {
        return $this->json([
            'message' => 'Taggart Jensen - Test for Carton Cloud',
        ]);
    }


  /**
   *  Just show output of TotalsCalculator()
   */
    public function testTotals() {
      $totals = new TotalsCalculator();
      $ids = [
        2344,
        2345,
      ];
      $data =  $totals->generateReport($ids);

      return new Response($data);
    }
}
