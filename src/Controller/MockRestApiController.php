<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\Client;

class MockRestApiController extends AbstractController
{

  /**
   * Carton Cloud basic auth user name.
   */
  const CCLOUD_USER = 'interview-test@cartoncloud.com.au';

  /**
   * Carton Cloud basic auth user name.
   */
  const CCLOUD_PASSWORD = 'test123456';

  /**
   * Carton Cloud basic auth string.
   */
  const CCLOUD_BASIC_AUTH_STRING = 'Basic aW50ZXJ2aWV3LXRlc3RAY2FydG9uY2xvdWQuY29tLmF1IDp0ZXN0MTIzNDU2';

  const CCLOUD_END_POINT = 'https://api.cartoncloud.com.au/CartonCloud_Demo/PurchaseOrders/';

  /**
   * @var RequestStack.
   */
  protected $requestStack;

  /**
   * @var GuzzleHttp\Client()
   */
  protected $client;

  /**
   * MockRestApiController constructor.
   *
   * @param RequestStack $requestStack
   *   The requestStack object.
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
    $this->client = new Client();
  }

  /**
   * Handels the Post Request to "/test"
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
    public function index() {
        $response = [];
        $status_code = 200;
        // Get current request.
        $request = $this->requestStack->getCurrentRequest();
        if ($request->getMethod() !== 'POST') {
          $response = ['error' => 'Invalid Http Method'];
          $status_code = 403; // No
        }
        else {
          if (!empty($request->getContent())) {
            $json = json_decode($request->getContent(), true);
            if (!empty($json)) {
              // Check for the paramater.
              if (!empty($json['purchase_order_ids'])) {

                $data = [];
                foreach ($json['purchase_order_ids'] as $id) {
                  // Ensure cast Int.
                  $order_id = (int) $id;
                  $get_purchase_order = $this->getPurchaseOrderById($order_id);
                  if (!empty($get_purchase_order) && !empty($get_purchase_order['data'])) {
                    $data[$order_id] = $get_purchase_order['data'];
                  }
                }

                if (!empty($data)) {
                  // Calculate totals.
                  $response = $this->calculateProductTotal($data);
                }
                else {
                  $response = ['error' => 'invalid PurchaseOrder ids'];
                  $status_code = 400;
                }
              }
            }
            // Make sure we error here if response is empty.
            if (empty($response)) {
              $response = ['error' => 'Invalid post data'];
              $status_code = 400; // bad request
            }
          }
          else {
            $response = ['error' => 'Invalid post data'];
            $status_code = 400; // bad request
          }
        }
        return $this->json($response, $status_code);
    }

  /**
   *  This gets the PurchaseOrder by id.
   * @param integer $id
   *   The Purchase Order id.
   * @return array
   *   This will return an array.
   */
    public function getPurchaseOrderById(int $id): array {
      $return = [];
      try  {
        $url = self::CCLOUD_END_POINT . $id .'?version=5&associated=true';
        // Async this.
        $promise = $this->client->getAsync($url, [
          'auth' => [
            self::CCLOUD_USER,
            self::CCLOUD_PASSWORD,
          ]
        ]);

        $response = $promise->wait();
        if ($response) {
          $body = $response->getBody()->getContents();
          if (!empty($body)) {
            $return = json_decode($body, TRUE);
          }
        }

      } catch (\Exception $e) {
        $return = ['error' => $e->getMessage()];
      }

      return $return;
    }


  /**
   *  Calculate product total.
   *
   * @param $data
   *   Array of product data.
   *
   * @return array
   *  Calculated array.
   *
   */
    public function calculateProductTotal(array $data): array {
      // Define $results.
      $results = [
        'result' => [
          [
            'product_type_id' => '1',
            'total' => 0,
          ],
          [
            'product_type_id' => '2',
            'total' => 0,
          ],
          [
            'product_type_id' => '3',
            'total' => 0,
          ],
        ],
      ];



      foreach ($data as $order_id => $order) {
        if (!empty($order['PurchaseOrderProduct'])) {
          foreach ($order['PurchaseOrderProduct'] as $order_data) {
             if (!empty($order_data['product_type_id'])) {
               $type_id = (string) $order_data['product_type_id'];
               $quantity = $order_data['unit_quantity_initial'];
               switch ($type_id) {
                 // This should not be static in code :( .
                 case "1":
                 case "3":
                   // Calculate by Weight.
                   //sum(unit_quantity_initial x Product.weight)
                   $weight = $order_data['Product']['weight'];
                   $total = $weight * $quantity;

                   if ($type_id == "1") {
                     $result_key = 0;
                   }
                   if ($type_id == "3") {
                     $result_key = 2;
                   }
                   $existing_total = $results['result'][$result_key]['total'];
                   $new_total = $existing_total + $total;
                   $results['result'][$result_key]['total'] = round($new_total, 2);

                   break;
                 case "2":
                   // Calculate by Volume.
                   // sum(unit_quantity_initial x Product.volume)
                   $volume =  $order_data['Product']['volume'];
                   $total =  $volume * $quantity;
                   $result_key = 1;
                   $existing_total = $results['result'][$result_key]['total'];
                   $new_total = $existing_total + $total;
                   $results['result'][$result_key]['total'] = round($new_total, 2);
                   break;
               }
             }
          }
        }
      }
      return $results;
    }
}
