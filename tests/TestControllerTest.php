<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use BearClaw\Warehousing\TotalsCalculator;
use App\Controller\MockRestApiController;
use Symfony\Component\HttpFoundation\RequestStack;

class TestControllerTest extends TestCase {

  /**
   * A test to see if we can get an instance of
   *
   * the TotalsCalculator class
   */
    public function testTotalsCalculator() {

      $class = new TotalsCalculator();
      if (method_exists($class, 'generateReport')) {
        $this->assertTrue(true);
      }
      else {
        $this->assertTrue(false);
      }
    }

  /**
   * A test to see if we load a new controller.
   *
   * the MockRestApiController class
   */
  public function testMockRestApiController() {
    $requestStack = new RequestStack();
    $class = new MockRestApiController($requestStack);
    if ($class::CCLOUD_USER === 'interview-test@cartoncloud.com.au') {
      $this->assertTrue(true);
    } else {
      $this->assertTrue(false);
    }

    // test api call
    $order_id = '2344';
    $data = $class->getPurchaseOrderById($order_id);
    if (!empty($data['info'])) {
      $this->assertTrue(true);
    }
    else {
        $this->assertTrue(false);
    }

  }

  /**
   * A test Controller api call.
   *
   */
  public function testMockRestApiControllerApiCall() {
    $requestStack = new RequestStack();
    $class = new MockRestApiController($requestStack);
    // Test api call
    $order_id = '2344';
    $data = $class->getPurchaseOrderById($order_id);
    if (!empty($data['info'])) {
      $this->assertTrue(true);
    }
    else {
      $this->assertTrue(false);
    }

  }
}
