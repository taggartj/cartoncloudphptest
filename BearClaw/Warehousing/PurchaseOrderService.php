<?php
namespace BearClaw\Warehousing;

use App\Controller\MockRestApiController;
use Symfony\Component\HttpFoundation\RequestStack;

class PurchaseOrderService extends MockRestApiController {


  public $requestStack;
  public function __construct() {
    $this->requestStack = new RequestStack();
    parent::__construct($this->requestStack);
  }

  public function calculateTotals($ids) {
     $data = [];
     foreach ($ids as $id) {
       // Cast int
       $order_id = (int) $id;
       $get_purchase_order = $this->getPurchaseOrderById($order_id);
       if (!empty($get_purchase_order) && !empty($get_purchase_order['data'])) {
         $data[$order_id] = $get_purchase_order['data'];
       }
     }

     $results = $this->calculateProductTotal($data);
     return $results['result'];
   }
}